import socket
import threading


class Server:
    def __init__(self):
        self.host = '127.0.0.1'
        self.port = 6060
        self.clients = list()
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.bind((self.host, self.port))
        self.server.listen()
        print(f'Listening on {self.host}')
        self.accept_client()

    def receive(self, client: socket.socket) -> None:
        while True:
            try:
                message = client.recv(1024)
                decoded_message = message.decode('utf-8')
                if '.doc' in decoded_message:
                    client.send(b'Server accepting word..\n')
                    f = open('./server/' + decoded_message, 'wb')
                    l = 1
                    while l:
                        l = client.recv(1024)
                        if l == b'END_FILE':
                            break

                        f.write(l)

                    print('Doc received')
                    f.close()

                else:
                    for el in self.clients:
                        el.send(message)
            except Exception as e:
                pass

    def accept_client(self) -> None:
        while True:
            client, addr = self.server.accept()
            self.clients.append(client)
            client.send('Connected\n'.encode('utf-8'))
            thread = threading.Thread(target=self.receive, args=(client,))
            thread.start()


if __name__ == '__main__':
    serv = Server()
