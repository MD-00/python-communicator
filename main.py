import os
import subprocess
import sys
from time import sleep

def main():
    client_number = int(input('Number of clients: '))
    if sys.platform == "win32":
        server_path = os.getcwd() + '\\server\\server.py'
        client_path = os.getcwd() + '\\client\\client.py'
        os.startfile(server_path)
        for i in range(client_number):
            os.startfile(client_path)
    else:
        server_path = os.getcwd() + '/server/server.py'
        client_path = os.getcwd() + '/client/client.py'
        subprocess.Popen(['gnome-terminal', '--', 'python3', server_path])
        sleep(0.5)
        for i in range(client_number):
            subprocess.Popen(['gnome-terminal', '--', 'python3', client_path])
            sleep(0.5)


main()
