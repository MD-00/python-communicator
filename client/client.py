import socket
import threading
from time import sleep


class Client:
    def __init__(self):
        self.host = '127.0.0.1'
        self.port = 6060
        self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client.connect((self.host, self.port))
        nick = input('nick: ')
        self.nickname = str(nick)
        r_thread = threading.Thread(target=self.receive, args=())
        r_thread.start()
        self.send()

    def receive(self) -> None:
        while True:
            r_message = self.client.recv(1024).decode('utf-8')
            print(r_message)

    def send(self) -> None:
        while True:
            s_message = input()
            if '.doc' in s_message:
                try:
                    f = open('./client/' + s_message, 'rb')
                    self.client.send(s_message.encode('utf-8'))
                    while True:
                        l = f.read(1024)
                        while (l):
                            self.client.send(l)
                            l = f.read(1024)
                        if l.decode('utf-8') == '':
                            sleep(1)
                            end_message = 'END_FILE'
                            self.client.send(end_message.encode('utf-8'))
                            f.close()
                            print('File sent')
                            break
                        break
                except Exception as e:
                    print(e)
            else:
                s_message = self.nickname + ': ' + s_message
                self.client.send(s_message.encode('utf-8'))


if __name__ == '__main__':
    my_client = Client()
